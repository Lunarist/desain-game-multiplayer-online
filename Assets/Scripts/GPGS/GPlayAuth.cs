﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GPlayAuth : MonoBehaviour
{
    public static GPlayAuth Instance { get; private set; }
    public static PlayGamesPlatform platform;
    public GameObject signin, menu;

    void Start()
    {
        Instance = this;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        platform = PlayGamesPlatform.Activate();
    }

    public void SignIn()
    {
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                if (success)
                {
                    Debug.Log("Success");
                    signin.SetActive(false);
                    menu.SetActive(true);
                }
                else
                {
                    Debug.Log("Failed");
                }
            }
        });
    }

    public void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
        menu.SetActive(false);
        signin.SetActive(true);
    }

    public void Leaderboard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI();
    }

    public static void AddScoreToLeaderboard(string leaderboardId, long score)
    {
        PlayGamesPlatform.Instance.ReportScore(score, leaderboardId, success => { });
    }
}
