﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class Player : NetworkBehaviour
{
    public float speed;
    public float increase;

    protected Joystick joystick;

    int score;
    int health;
    Rigidbody rb;

    Text scoreText;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        joystick = FindObjectOfType<Joystick>();
        
    }

    public override void OnStartLocalPlayer()
    {
        Camera.main.GetComponent<CameraFollow>().setTarget(gameObject.transform);

        score = 0;
        health = 10;
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = "Score : " + score.ToString();
    }

    [Client]
    private void Update()
    {
        if (!hasAuthority) { return; }

        if (!Input.anyKey) { return; }

        if (!joystick) { return; }

        Cmd();
    }

    [Command]
    void Cmd()
    {
        RpcMove();
    }

    [ClientRpc]
    void RpcMove()
    {
        Vector3 move = new Vector3(
             joystick.Horizontal * speed * Time.deltaTime,
             joystick.Vertical * speed * Time.deltaTime,
             0);
        rb.MovePosition(transform.position + move);

        if (transform.position.x >= 22.15f)
            transform.position = new Vector3(22.15f, transform.position.y, transform.position.z);
        else if (transform.position.x <= -22.15f)
            transform.position = new Vector3(-22.15f, transform.position.y, transform.position.z);

        if (transform.position.y >= 12.25f)
            transform.position = new Vector3(transform.position.x, 12.25f, transform.position.z);
        else if (transform.position.y <= -12.25f)
            transform.position = new Vector3(transform.position.x, -12.25f, transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isLocalPlayer)
        {
            // Collide with food (For Client)
            if (other.gameObject.tag == "Food")
            {
                transform.localScale += new Vector3(increase, increase, increase);

                score++;
                scoreText.text = "Score : " + score.ToString();

                health++;
            }

            // Collide with other player
            if (other.gameObject.tag == "Player")
            {
                health--;
                Debug.Log(health.ToString());

                if (health <= 0)
                {
                    GameResult.instance.GetScore(score);
                    GameResult.instance.ShowResult();

                    Connect conn = GetComponent<Connect>();
                    conn.StopGame();
                }
                else
                {
                    transform.localScale += new Vector3(-increase, -increase, -increase);
                }
            }
        }

        if (isServer)
        {
            // Collide with food (For Server)
            if (other.gameObject.tag == "Food")
            {
                Spawn.totalFood--;
                NetworkServer.Destroy(other.gameObject);
            }
        }
    }
}
