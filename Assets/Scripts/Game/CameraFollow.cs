﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;

    public float smoothTime = 0.15f;

    private void Update()
    {
        transform.position = new Vector3(
            Mathf.Clamp(player.position.x, -13.5f, 13.5f),
            Mathf.Clamp(player.position.y, -7.5f, 7.5f),
            transform.position.z);
    }

    public void setTarget(Transform target)
    {
        player = target;
    }
}
