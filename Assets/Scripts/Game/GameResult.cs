﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Mirror;

public class GameResult : NetworkBehaviour
{
    public static GameResult instance;
    public static int currScore;
    public static bool isDeath;
    
    public Text scoreText;
    public Text highscoreText;

    public GameObject result;
    public GameObject playerPrefab;

    void Start()
    {
        instance = this;
    }

    public void ShowResult()
    {
        result.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void Home()
    {
        NetworkClient.Shutdown();
        NetworkManager.singleton.StopClient();
        result.SetActive(false);
        SceneManager.LoadScene("Menu");
    }

    public void GetScore(int score)
    {
        currScore = score;
        scoreText.text = currScore.ToString();

        int highscore = PlayerPrefs.GetInt("Highscore");
        if (highscore == 0)
        {
            GPlayAuth.AddScoreToLeaderboard("Highscore", currScore);
        }

        if (highscore < currScore)
        {
            highscore = currScore;
            PlayerPrefs.SetInt("Highscore", highscore);
            GPlayAuth.AddScoreToLeaderboard("Highscore", highscore);
        }
        highscoreText.text = highscore.ToString();
    }
}
